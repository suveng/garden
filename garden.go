package garden

import "log"

type User struct {
}

type IUser interface {
	say()
}

func (u *User) say() {
	log.Println("say something...")
}

func init() {
	log.Println("welcome to use garden, if you have some issues , please contact me  suveng@163.com")
	log.Println("garden official web address : http://gitee.com/suveng/garden")
}
